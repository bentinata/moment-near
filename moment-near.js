(function() {
  if (typeof require !== "undefined" && require !== null) {
    moment = require('moment');
  }
  else {
    moment = this.moment;
  }

  moment.fn.near = function(unit) {
    var format = '';

    switch (unit) {
      case 'seconds':
        format = ':ss' + format;
      case 'minutes':
        format = ':mm' + format;
      case 'hours':
        format = 'THH' + format;
      case 'days':
        format = '-DD' + format;
      case 'months':
        format = '-MM' + format;
      case 'years':
        format = 'YYYY' + format;
        break;
      default:
        format = 'YYYY-MM-DDTHH:mm:ss';
    }

    return this.format(format)
  };

  if (typeof module !== "undefined" && module !== null) {
    module.exports = moment;
  }
  else {
    this.moment = moment;
  }
}).call(this)